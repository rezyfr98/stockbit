object Modules {
    const val app = ":app"
    const val core = ":core"

    const val auth = ":feature:auth"
    const val navigation = ":feature:navigation"
    const val watchlist = ":feature:watchlist"
}
