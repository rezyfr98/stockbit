object Versions {

    const val kotlinVersion = "1.3.71"

    const val coreVersion = "1.2.0"
    const val appCompatVersion = "1.1.0"
    const val constraintVersion = "1.1.3"
    const val preferenceVersion = "1.1.0"

    const val coroutinesVersion = "1.3.3"
    const val lifecycleVersion = "2.2.0"
    const val navigationVersion = "2.3.0-alpha04"
    const val koinVersion = "2.1.4"
    const val roomVersion = "2.2.5"
    const val materialVersion = "1.3.0-alpha02"
    const val swipeToRefreshVersion = "1.1.0"

    const val okHttpVersion = "4.6.0"
    const val gsonVersion = "2.8.6"

    const val retrofit_version = "2.8.1"

    const val junitVersion = "4.12"
    const val mockWebServerVersion = "4.2.1"
    const val truthVersion = "1.0"
    const val archComponentTestVersion = "2.1.0"
    const val androidxJunitVersion = "1.1.1"

}
