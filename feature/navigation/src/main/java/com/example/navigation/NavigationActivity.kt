package com.example.navigation

import com.example.core.presentation.base.BaseActivity

class NavigationActivity : BaseActivity() {
    override fun layoutRes() = R.layout.activity_navigation

    override fun haveToolbar(): Boolean = true

    override fun showBackButton(): Boolean = true

    override fun initView() {}
}