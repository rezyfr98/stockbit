package com.example.watchlist

import com.example.core.data.repositories.CryptoRepositoryImpl
import com.example.watchlist.helper.BaseTest
import com.example.watchlist.views.watchlist.CryptoViewModel
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

@ExperimentalCoroutinesApi
internal class CryptoViewModelTest : BaseTest() {

    private lateinit var cryptoViewModel: CryptoViewModel

    @Test
    fun `viewmodel watchlist`() {
        coroutineTestRule.dispatcher.runBlockingTest {
            prepareViewModel()

            cryptoViewModel.getWatchlist(1)
            cryptoViewModel.stateCrypto.observeForever { stateWatchlist ->
                Truth.assertThat(stateWatchlist).isNotNull()
            }

        }
    }

    @Test
    fun `viewmodel watchlist error`() {
        coroutineTestRule.dispatcher.runBlockingTest {
            prepareViewModel()
            cryptoViewModel.getWatchlist(-1)
            cryptoViewModel.stateCrypto.observeForever { stateWatchlist ->
                Truth.assertThat(stateWatchlist).isNotNull()
            }
        }
    }

    private fun prepareViewModel() {
        val watchlistRepository = CryptoRepositoryImpl(watchlistApi)
        cryptoViewModel = CryptoViewModel(watchlistRepository)
    }
}