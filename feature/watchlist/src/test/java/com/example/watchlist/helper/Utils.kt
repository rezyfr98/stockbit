package com.example.watchlist.helper

import com.google.common.io.Resources.getResource
import java.io.File

fun getJson(path: String): String {
    val uri = getResource(path)
    val file = File(uri.path)
    return String(file.readBytes())
}