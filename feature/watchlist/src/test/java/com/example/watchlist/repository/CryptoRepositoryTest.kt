package com.example.watchlist.repository

import com.example.core.data.repositories.CryptoRepository
import com.example.core.data.repositories.CryptoRepositoryImpl
import com.example.watchlist.helper.BaseTest
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

internal class CryptoRepositoryTest : BaseTest() {

    private lateinit var cryptoRepository: CryptoRepository

    @Before
    override fun setup() {
        super.setup()
        cryptoRepository = CryptoRepositoryImpl(watchlistApi)
    }

    @Test
    fun `fetch crypto`() {
        runBlocking {
            val cryptoData = cryptoRepository.getCryptoList(1)

            cryptoData.let {
                Truth.assertThat(it.body()?.Data?.size).isAtLeast(1)
            }
        }
    }
}