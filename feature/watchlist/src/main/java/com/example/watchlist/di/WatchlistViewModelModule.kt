package com.example.watchlist.di

import com.example.watchlist.views.watchlist.CryptoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val watchlistViewModelModule = module {
    viewModel { CryptoViewModel(get()) }
}