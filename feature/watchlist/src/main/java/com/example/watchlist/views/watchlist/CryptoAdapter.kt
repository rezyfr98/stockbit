package com.example.watchlist.views.watchlist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.core.domain.model.crypto.Data
import com.example.watchlist.R
import kotlinx.android.synthetic.main.item_watchlist.view.*
import java.math.RoundingMode
import java.text.DecimalFormat

class CryptoAdapter(
        private val listItem: ArrayList<Data>,
        private val listener: (Data) -> Unit
) : RecyclerView.Adapter<CryptoAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_watchlist, parent, false)
        return ItemViewHolder(view, parent.context)
    }

    override fun getItemCount(): Int = listItem.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int) = position

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindItem(listItem[position], listener)
    }

    inner class ItemViewHolder(v: View, private val context: Context) : RecyclerView.ViewHolder(v) {
        fun bindItem(data: Data, listener: (Data) -> Unit) {
            val priceFormat = DecimalFormat("#,###")
            val percentageFormat = DecimalFormat("#,###.##")

            val price = data.RAW?.IDR?.PRICE ?: 0.0
            val change = data.RAW?.IDR?.CHANGEHOUR ?: 0.0
            val changedPercentage = (change / price * 100).toFloat()

            percentageFormat.roundingMode = RoundingMode.CEILING
            itemView.txt_full_name.text = data.CoinInfo?.FullName
            itemView.txt_name.text = data.CoinInfo?.Name
            itemView.txt_price.text = priceFormat.format(price)
            when {
                change < 0 -> {
                    itemView.txt_change.text =
                            "${priceFormat.format(change)} (${percentageFormat.format(changedPercentage)}%)"
                    itemView.txt_change.setTextColor(
                            ContextCompat.getColor(
                                    context,
                                    R.color.colorRed
                            )
                    )
                }
                change > 0 -> {
                    itemView.txt_change.text =
                            "${priceFormat.format(change)} (${percentageFormat.format(changedPercentage)}%)"
                    itemView.txt_change.setTextColor(
                            ContextCompat.getColor(
                                    context,
                                    R.color.colorPrimary
                            )
                    )
                }
                else -> {
                    itemView.txt_change.text = "0 (0.00%)"
                }
            }

            itemView.setOnClickListener {
                listener(data)
            }
        }
    }

    fun setData(data: List<Data>) {
//        listItem.clear()
        listItem.addAll(data)
        notifyDataSetChanged()
    }

}