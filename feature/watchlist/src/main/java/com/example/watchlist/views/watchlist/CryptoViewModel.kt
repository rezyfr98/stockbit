package com.example.watchlist.views.watchlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.core.data.repositories.CryptoRepository
import com.example.core.domain.model.crypto.Data
import kotlinx.coroutines.launch

class CryptoViewModel(
        private val cryptoRepository: CryptoRepository
) : ViewModel() {
    private val _stateCrypto = MutableLiveData<StateCrypto>()
    val stateCrypto: LiveData<StateCrypto> get() = _stateCrypto

    fun getWatchlist(page: Int) {
        viewModelScope.launch {
            runCatching {
                _stateCrypto.postValue(
                        StateCrypto.Loading(
                                "Loading"
                        )
                )
                cryptoRepository.getCryptoList(page)
            }.onSuccess {
                _stateCrypto.postValue(it.body()?.Data?.let { it1 ->
                    StateCrypto.ShowCrypto(
                            it1
                    )
                })
            }.onFailure {
                _stateCrypto.postValue(
                        StateCrypto.Error(
                                it.message.toString()
                        )
                )
            }
        }
    }

    sealed class StateCrypto {
        data class ShowCrypto(val data: List<Data>) : StateCrypto()
        data class Loading(val message: String) : StateCrypto()
        data class Error(val message: String) : StateCrypto()
    }
}