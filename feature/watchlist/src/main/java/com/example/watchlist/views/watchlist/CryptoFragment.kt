package com.example.watchlist.views.watchlist

import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.core.domain.model.crypto.Data
import com.example.core.presentation.base.BaseFragment
import com.example.watchlist.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.watchlist_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CryptoFragment : BaseFragment() {

    private lateinit var cryptoAdapter: CryptoAdapter
    private val viewModel: CryptoViewModel by viewModel()
    private var isLoading = false
    private val page = 1

    override fun toolbarWatchlist(): Boolean = true

    override fun layoutRes(): Int =
            R.layout.watchlist_fragment

    override fun initView() {
        this.activity?.findViewById<BottomNavigationView>(R.id.nav_bottom)?.visibility = View.VISIBLE
        var newPage = 1
        viewModel.getWatchlist(page)
        cryptoAdapter = CryptoAdapter(arrayListOf()) { it ->
            onItemClicked(it)
        }
        rv_watchlist.adapter = cryptoAdapter
        rv_watchlist.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = rv_watchlist.layoutManager as LinearLayoutManager
                val countItem = layoutManager.itemCount
                val lastVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition()
                val isLastPosition = countItem.minus(1) == lastVisiblePosition
                if (!isLoading && isLastPosition) {
                    if (countItem >= 50) {
                        Toast.makeText(activity, "Data sudah maksimal", Toast.LENGTH_SHORT).show()
                    } else {
                        newPage += 1
                        viewModel.getWatchlist(newPage)
                    }
                }
            }
        })
        srl_refresh.setOnRefreshListener {
            viewModel.getWatchlist(page)
            srl_refresh.isRefreshing = false
        }
        requireActivity().onBackPressedDispatcher.addCallback(
                this,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        activity?.finish()
                    }
                }
        )
    }

    override fun observeData() {
        viewModel.stateCrypto.observe(viewLifecycleOwner, Observer {
            when (it) {
                is CryptoViewModel.StateCrypto.ShowCrypto -> {
                    Log.d("Response: ", "${it.data}")
                    srl_refresh.visibility = View.VISIBLE
                    val params = progress_circular.layoutParams as RelativeLayout.LayoutParams
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                    progress_circular.layoutParams = params
                    cryptoAdapter.setData(it.data)
                    isLoading = false
                    stateLoading(isLoading)
                }
                is CryptoViewModel.StateCrypto.Loading -> {
                    isLoading = true
                    stateLoading(isLoading)
                }
                is CryptoViewModel.StateCrypto.Error -> {
                    Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun onItemClicked(data: Data) {
        Toast.makeText(activity, data.CoinInfo?.Name, Toast.LENGTH_LONG).show()
    }

    private fun stateLoading(isLoading: Boolean) {
        if (isLoading) {
            progress_circular.visibility = View.VISIBLE
        } else {
            progress_circular.visibility = View.GONE
        }
    }

}