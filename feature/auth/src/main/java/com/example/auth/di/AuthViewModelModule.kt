package com.example.auth.di

import com.example.auth.views.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val authViewModelModule = module {
    viewModel { LoginViewModel() }
}