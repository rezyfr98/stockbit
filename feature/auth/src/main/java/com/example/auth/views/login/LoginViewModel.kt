package com.example.auth.views.login

import androidx.lifecycle.ViewModel
import com.example.core.SingleLiveEvent

class LoginViewModel : ViewModel() {

    val loginState = SingleLiveEvent<LoginState>()

    fun doLogin(username: String, password: String) {
        if (username.isNotEmpty() && password.isNotEmpty()) {
            loginState.postValue(
                    LoginState.Success(
                            "Sukses Login"
                    )
            )
        } else {
            loginState.postValue(LoginState.Error("The user field is required\nThe password field is required"))
        }
    }
}

sealed class LoginState {
    data class Error(val message: String?) : LoginState()
    data class Success(val message: String?) : LoginState()
}