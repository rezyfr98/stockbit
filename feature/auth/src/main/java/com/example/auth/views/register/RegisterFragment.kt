package com.example.auth.views.register

import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.example.auth.R
import com.example.core.presentation.base.BaseFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class RegisterFragment : BaseFragment() {

    override fun layoutRes(): Int = R.layout.register_fragment

    override fun haveBottomNav(): Boolean = false

    override fun titleRes(): Int? = R.string.title_daftar

    override fun initView() {
        this.activity?.findViewById<BottomNavigationView>(R.id.nav_bottom)?.visibility = View.GONE
        requireActivity().onBackPressedDispatcher.addCallback(
                this,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        findNavController().popBackStack()
                    }
                }
        )
    }

    override fun observeData() {

    }

}