package com.example.auth.views.login

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.auth.R
import com.example.core.presentation.base.BaseFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.login_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment() {

    private val viewModel: LoginViewModel by viewModel()

    override fun layoutRes(): Int = R.layout.login_fragment

    override fun haveBottomNav(): Boolean = false

    override fun titleRes(): Int? = R.string.title_masuk

    override fun initView() {
        this.activity?.findViewById<BottomNavigationView>(R.id.nav_bottom)?.visibility = View.GONE
        container_register.setOnClickListener {
            findNavController().navigate(R.id.loginFragment_to_registerFragment)
        }

        btn_login.setOnClickListener {
            viewModel.doLogin(
                    et_email.text.toString(),
                    et_password.text.toString()
            )
        }
    }

    override fun observeData() {
        viewModel.loginState.observe(this, Observer {
            when (it) {
                is LoginState.Success -> {
                    findNavController().navigate(R.id.loginFragment_to_cryptoFragment)
                    Log.d("Login: ", "${it.message}")
                }
                is LoginState.Error -> {
                    Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}