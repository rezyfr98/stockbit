package com.example.stockbittest.di

import com.example.auth.di.authViewModelModule
import com.example.watchlist.di.watchlistViewModelModule

val appViewModelModule = listOf(
        authViewModelModule,
        watchlistViewModelModule
)
