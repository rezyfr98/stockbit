package com.example.stockbittest.di

import com.example.core.data.di.dataModule
import com.example.core.data.di.helperModule
import com.example.core.data.di.networkModule

val appDataModule = listOf(
        dataModule,
        networkModule,
        helperModule
)
