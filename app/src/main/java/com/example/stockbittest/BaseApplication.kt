package com.example.stockbittest

import android.app.Application
import com.example.stockbittest.di.appDataModule
import com.example.stockbittest.di.appViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BaseApplication : Application() {

//    private val preferences: LocalDataSource.Preferences by inject()

    override fun onCreate() {
        super.onCreate()
        startInject()
    }

    private fun startInject() {
        val appModules = appDataModule + appViewModelModule
        startKoin {
            androidLogger()
            androidContext(this@BaseApplication)
            modules(appModules)
        }
    }
}
