package com.example.stockbittest.views

import android.content.Intent
import com.example.core.presentation.base.BaseActivity
import com.example.navigation.NavigationActivity
import com.example.stockbittest.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    override fun layoutRes(): Int = R.layout.activity_main

    override fun initView() {
        btn_login.setOnClickListener {
            startActivity(Intent(this, NavigationActivity::class.java))
        }
    }
}