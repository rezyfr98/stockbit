package com.example.core.domain.model.crypto

import androidx.room.Entity
import androidx.room.PrimaryKey

data class Crypto(
        var Data: List<Data>,
        var HasWarning: Boolean,
        var Message: String,
        var SponsoredData: List<Any>,
        var Type: Int
)

@Entity
data class Data(
        @PrimaryKey(autoGenerate = false) var id: Int = 0,
        var CoinInfo: CoinInfo? = null,
        var DISPLAY: Display? = null,
        var RAW: Raw? = null
)

data class CoinInfo(
        var Algorithm: String,
        var FullName: String,
        var Id: String,
        var ImageUrl: String,
        var Internal: String,
        var Name: String,
        var Type: Int
)

data class Display(
        var IDR: IdrDisplay
)

data class IdrDisplay(
        var CHANGE24HOUR: String,
        var CHANGEDAY: String,
        var CHANGEHOUR: String,
        var PRICE: String,
        var TOSYMBOL: String
)

data class Raw(
        var IDR: IdrRaw
)

data class IdrRaw(
        var PRICE: Double,
        var CHANGEHOUR: Double
)