package com.example.core.presentation.base

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    protected abstract fun layoutRes(): Int

    @MenuRes
    protected open fun menuRes(): Int? = null

    @StringRes
    open fun titleRes(): Int? = null

    open fun haveToolbar(): Boolean = false

    protected open fun showBackButton(): Boolean = false

    abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes())

        titleRes()?.let { toolbar_title.text = resources.getString(it) }

        if (haveToolbar()) {
            if (showBackButton()) {
                btn_back.visibility = View.VISIBLE
                btn_back.setOnClickListener {
                    onBackPressed()
                }
            } else {
                btn_back.visibility = View.GONE
            }
        }

        initView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuRes()?.let {
            menuInflater.inflate(it, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

}