package com.example.core.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.example.core.R
import com.google.android.material.bottomnavigation.BottomNavigationView

abstract class BaseFragment : Fragment() {

    @LayoutRes
    protected abstract fun layoutRes(): Int

    @StringRes
    protected open fun titleRes(): Int? = null

    protected open fun toolbarWatchlist(): Boolean = false

    protected open fun haveBottomNav(): Boolean = false

    abstract fun initView()

    abstract fun observeData()

    open fun initData() {

    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(layoutRes(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleRes()?.let {
            activity?.findViewById<TextView>(R.id.toolbar_title)?.text = resources.getString(it)
        }
        if (toolbarWatchlist()) {
            activity?.findViewById<RelativeLayout>(R.id.view_watchlist)?.visibility = View.VISIBLE
            activity?.findViewById<RelativeLayout>(R.id.view_auth)?.visibility = View.GONE
        } else {
            activity?.findViewById<RelativeLayout>(R.id.view_watchlist)?.visibility = View.GONE
        }
        initData()
        initView()
        observeData()
    }


}
