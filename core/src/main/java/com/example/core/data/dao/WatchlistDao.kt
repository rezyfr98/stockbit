package com.example.core.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.core.domain.model.crypto.Data

@Dao
interface WatchlistDao : BaseDao<Data> {
    @Query("SELECT * FROM `Data`")
    suspend fun getCategories(): List<Data>
}