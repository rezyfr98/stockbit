package com.example.core.data.repositories

import com.example.core.data.api.WatchlistApi
import com.example.core.domain.model.crypto.Crypto
import retrofit2.Response

interface CryptoRepository {
    suspend fun getCryptoList(page: Int): Response<Crypto>
}

class CryptoRepositoryImpl(
        private val watchlistApi: WatchlistApi
) : CryptoRepository {

    override suspend fun getCryptoList(page: Int): Response<Crypto> {
        return watchlistApi.getCryptoList(page)
    }
}