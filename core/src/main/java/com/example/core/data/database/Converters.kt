package com.example.core.data.database

import androidx.room.TypeConverter
import com.example.core.domain.model.crypto.CoinInfo
import com.example.core.domain.model.crypto.Data
import com.example.core.domain.model.crypto.Display
import com.example.core.domain.model.crypto.Raw
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {

    @TypeConverter
    fun storedStringToCoinInfo(value: String): CoinInfo {
        val type = object : TypeToken<CoinInfo>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun coinInfoToStoredString(list: CoinInfo): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun storedStringToDisplay(value: String): Display {
        val type = object : TypeToken<Display>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun displayToStoredString(list: Display): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun storedStringToData(value: String): List<Data> {
        val type = object : TypeToken<List<Data>>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun dataToStoredString(list: List<Data>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun storedStringToRaw(value: String): Raw {
        val type = object : TypeToken<Raw>() {}.type
        return Gson().fromJson(value, type)
    }

    @TypeConverter
    fun rawToStoredString(list: Raw): String {
        return Gson().toJson(list)
    }
}
