package com.example.core.data.di

import androidx.room.Room
import com.example.core.data.dao.WatchlistDao
import com.example.core.data.database.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.scope.Scope
import org.koin.dsl.module

val dataModule = module {

    single { provideDb() }

}

private fun Scope.provideDb() =
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "STOCKBIT_TEST_DB").build()

