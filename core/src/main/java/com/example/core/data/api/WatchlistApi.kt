package com.example.core.data.api

import com.example.core.domain.model.crypto.Crypto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WatchlistApi {
    @GET("data/top/totaltoptiervolfull?limit=10&tsym=IDR")
    suspend fun getCryptoList(
            @Query("page") page: Int
    ): Response<Crypto>
}