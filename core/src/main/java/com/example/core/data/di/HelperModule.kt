package com.example.core.data.di

import com.example.core.data.repositories.CryptoRepository
import com.example.core.data.repositories.CryptoRepositoryImpl
import org.koin.dsl.module

val helperModule = module {
    single<CryptoRepository> {
        CryptoRepositoryImpl(get())
    }
}